#!/usr/bin/python2

import os
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice
from pdfminer.pdfpage import PDFTextExtractionNotAllowed
from pdfminer.layout import LAParams, LTTextBox, LTTextLine
from pdfminer.converter import PDFPageAggregator

import pandas as pd
from rake_nltk import Metric, Rake
from nltk.corpus import stopwords
import nltk

#path_to_pdf_file=raw_input("Enter path to pdf file:")
my_file = os.path.join("JavaBasics-notes.pdf")#replace "JavaBasics-notes.pdf" with "path_to_pdf_file"
extracted_text = ""
fp = open(my_file, "rb")
parser =PDFParser(fp)
document = PDFDocument(parser)
if not document.is_extractable:
	raise PDFTextExtractionNotAllowed
rsrcmgr = PDFResourceManager()
laparams = LAParams()
device = PDFPageAggregator(rsrcmgr, laparams=laparams)
interpreter = PDFPageInterpreter(rsrcmgr, device)
for page in PDFPage.create_pages(document):
	interpreter.process_page(page)
	layout = device.get_result()
	for lt_obj in layout:
		if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
			extracted_text += lt_obj.get_text()
fp.close()		


r = Rake(
	ranking_metric=Metric.WORD_FREQUENCY,
	stopwords=set(stopwords.words('english'))
) 
a=r.extract_keywords_from_text(extracted_text)
#b=r.get_ranked_phrases()
word_with_weightage=r.get_ranked_phrases_with_scores()
#print(b)
#print(c)



df = pd.DataFrame(word_with_weightage, columns=['word weight', 'Words'])
#print df

df.to_csv('output.csv',  sep='\t', index=True,  encoding='utf-8') #output.csv is name of excel file in which o/p is save



